#include "z0_des.h"

#include <stdio.h>
#include <string.h>

static const uint8_t pc1[] = {
	57, 49, 41, 33, 25, 17,  9,
	 1, 58, 50, 42, 34, 26, 18,
	10,  2, 59, 51, 43, 35, 27,
	19, 11,  3, 60, 52, 44, 36,
	63, 55, 47, 39, 31, 23, 15,
	 7, 62, 54, 46, 38, 30, 22,
	14,  6, 61, 53, 45, 37, 29,
	21, 13,  5, 28, 20, 12,  4
};

static const uint8_t ls[] = {
	1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1
};

static const uint8_t pc2[] = {
	14, 17, 11, 24,  1,  5,
	 3, 28, 15,  6, 21, 10,
	23, 19, 12,  4, 26,  8,
	16,  7, 27, 20, 13,  2,
	41, 52, 31, 37, 47, 55,
	30, 40, 51, 45, 33, 48,
	44, 49, 39, 56, 34, 53,
	46, 42, 50, 36, 29, 32
};

static const uint8_t ip[] = {
	58, 50, 42, 34, 26, 18, 10, 2,
	60, 52, 44, 36, 28, 20, 12, 4,
	62, 54, 46, 38, 30, 22, 14, 6,
	64, 56, 48, 40, 32, 24, 16, 8,
	57, 49, 41, 33, 25, 17, 9 , 1,
	59, 51, 43, 35, 27, 19, 11, 3,
	61, 53, 45, 37, 29, 21, 13, 5,
	63, 55, 47, 39, 31, 23, 15, 7
};

static const uint8_t e[] = {
	32,  1,  2,  3,  4,  5,
	 4,  5,  6,  7,  8,  9,
	 8,  9, 10, 11, 12, 13,
	12, 13, 14, 15, 16, 17,
	16, 17, 18, 19, 20, 21,
	20, 21, 22, 23, 24, 25,
	24, 25, 26, 27, 28, 29,
	28, 29, 30, 31, 32,  1
};

static const uint8_t s1[] = {
	14,  4, 13,  1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9,  0,  7,  
     	 0, 15,  7,  4, 14,  2, 13,  1, 10,  6, 12, 11,  9,  5,  3,  8,  
	 4,  1, 14,  8, 13,  6,  2, 11, 15, 12,  9,  7,  3, 10,  5,  0, 
    	15, 12,  8,  2,  4,  9,  1,  7,  5, 11,  3, 14, 10,  0,  6, 13
};

static const uint8_t s2[] = {
	15,  1,  8, 14,  6, 11,  3,  4,  9,  7,  2, 13, 12,  0,  5, 10,  
	 3, 13,  4,  7, 15,  2,  8, 14, 12,  0,  1, 10,  6,  9, 11,  5,  
	 0, 14,  7, 11, 10,  4, 13,  1,  5,  8, 12,  6,  9,  3,  2, 15, 
	13,  8, 10,  1,  3, 15,  4,  2, 11,  6,  7, 12,  0,  5, 14,  9
};

static const uint8_t s3[] = {
	10,  0,  9, 14,  6,  3, 15,  5,  1, 13, 12,  7, 11,  4,  2,  8,  
	13,  7,  0,  9,  3,  4,  6, 10,  2,  8,  5, 14, 12, 11, 15,  1,  
	13,  6,  4,  9,  8, 15,  3,  0, 11,  1,  2, 12,  5, 10, 14,  7,
	 1, 10, 13,  0,  6,  9,  8,  7,  4, 15, 14,  3, 11,  5,  2, 12
};

static const uint8_t s4[] = {
	 7, 13, 14,  3,  0,  6,  9, 10,  1,  2,  8,  5, 11, 12,  4, 15,  
	13,  8, 11,  5,  6, 15,  0,  3,  4,  7,  2, 12,  1, 10, 14,  9,  
	10,  6,  9,  0, 12, 11,  7, 13, 15,  1,  3, 14,  5,  2,  8,  4,
	 3, 15,  0,  6, 10,  1, 13,  8,  9,  4,  5, 11, 12,  7,  2, 14
};

static const uint8_t s5[] = {
	 2, 12,  4,  1,  7, 10, 11,  6,  8,  5,  3, 15, 13,  0, 14,  9, 
	14, 11,  2, 12,  4,  7, 13,  1,  5,  0, 15, 10,  3,  9,  8,  6, 
	 4,  2,  1, 11, 10, 13,  7,  8, 15,  9, 12,  5,  6,  3,  0, 14, 
	11,  8, 12,  7,  1, 14,  2, 13,  6, 15,  0,  9, 10,  4,  5,  3
};

static const uint8_t s6[] = {
	12,  1, 10, 15,  9,  2,  6,  8,  0, 13,  3,  4, 14,  7,  5, 11,
	10, 15,  4,  2,  7, 12,  9,  5,  6,  1, 13, 14,  0, 11,  3,  8,
	 9, 14, 15,  5,  2,  8, 12,  3,  7,  0,  4, 10,  1, 13, 11,  6,
	 4,  3,  2, 12,  9,  5, 15, 10, 11, 14,  1,  7,  6,  0,  8, 13
};

static const uint8_t s7[] = {
	 4, 11,  2, 14, 15,  0,  8, 13,  3, 12,  9,  7,  5, 10,  6,  1,
	13,  0, 11,  7,  4,  9,  1, 10, 14,  3,  5, 12,  2, 15,  8,  6,
	 1,  4, 11, 13, 12,  3,  7, 14, 10, 15,  6,  8,  0,  5,  9,  2,
	 6, 11, 13,  8,  1,  4, 10,  7,  9,  5,  0, 15, 14,  2,  3, 12
};

static const uint8_t s8[] = {
	13,  2,  8,  4,  6, 15, 11,  1, 10,  9,  3, 14,  5,  0, 12,  7,
	 1, 15, 13,  8, 10,  3,  7,  4, 12,  5,  6, 11,  0, 14,  9,  2,
	 7, 11,  4,  1,  9, 12, 14,  2,  0,  6, 10, 13, 15,  3,  5,  8,
	 2,  1, 14,  7,  4, 10,  8, 13, 15, 12,  9,  0,  3,  5,  6, 11
};

static const uint8_t p[] = {
	16,  7, 20, 21,
	29, 12, 28, 17,
	 1, 15, 23, 26,
	 5, 18, 31, 10,
	 2,  8, 24, 14,
	32, 27,  3,  9,
	19, 13, 30,  6,
	22, 11,  4, 25
};

static const uint8_t pi[] = {
	40, 8, 48, 16, 56, 24, 64, 32,
	39, 7, 47, 15, 55, 23, 63, 31,
	38, 6, 46, 14, 54, 22, 62, 30,
	37, 5, 45, 13, 53, 21, 61, 29,
	36, 4, 44, 12, 52, 20, 60, 28,
	35, 3, 43, 11, 51, 19, 59, 27,
	34, 2, 42, 10, 50, 18, 58, 26,
	33, 1, 41,  9, 49, 17, 57, 25
};

static void
z0_des_schedule_keys(uint64_t key, uint64_t keys[16])
{
	uint_fast8_t i;

	uint64_t pc1_res;
	pc1_res = 0;

	for (i = 0; i < 56; ++i) {
		if (key & (1UL << (64 - pc1[i])))
			pc1_res |= 1UL << (55 - i);	
	}

	uint_fast32_t c;
	c = pc1_res & 0x00FFFFFFF0000000;
	c = c >> 28;
	uint_fast32_t d;
	d = pc1_res & 0x000000000FFFFFFF;

	uint_fast8_t j;

	memset(keys, 0, sizeof (uint64_t) * 16);

	for (i = 0; i < 16; ++ i) {
		c  = (c << ls[i]) | (c >> (28 - ls[i]));
		c &= 0xFFFFFFF;
		d  = (d << ls[i]) | (d >> (28 - ls[i]));
		d &= 0xFFFFFFF;

		uint64_t merge;
		merge = ((uint64_t)c << 28) | d;
		for (j = 0; j < 48; ++j) {
			if (merge & (1UL << (56 - pc2[j])))
				keys[i] |= 1UL << (47 - j);
		}
	}	

}

uint64_t
z0_des_enc(uint64_t block, uint64_t key)
{
	uint64_t keys[16];
	z0_des_schedule_keys(key, keys);

	uint64_t ip_res;
	ip_res = 0;

	uint_fast8_t i;
	for (i = 0; i < 64; ++i) {
		if (block & (1UL << (64 - ip[i])))
			ip_res |= 1UL << (63 - i);
	}

	uint_fast32_t l0;
	l0 = ip_res & 0xFFFFFFFF00000000;
	l0 = l0 >> 32;
	uint_fast32_t r0;
	r0 = ip_res & 0x00000000FFFFFFFF;	

	uint_fast32_t l;
	uint_fast32_t r;
	for (i = 0; i < 16; ++i) {
		l = r0;
		r = 0;

		uint_fast8_t j;
		for (j = 0; j < 48; ++j) {
			if (r0 & (1UL << (32 - e[j])))
				r |= 1UL << (47 - j);
		}
		r = r ^ keys[i];
		
		uint_fast8_t row;
		uint_fast8_t col;

		uint_fast32_t part_1;
		part_1 = (r & 0x0000FC0000000000) >> 42;

		row = ((part_1 & 0b100000) >> 4) | (part_1 & 1);
		col = (part_1 & 0b011110) >> 1;

		part_1 = s1[row * 16 + col];

		uint_fast32_t part_2;
		part_2 = (r & 0x000003F000000000) >> 36;

		row = ((part_2 & 0b100000) >> 4) | (part_2 & 1);
		col = (part_2 & 0b011110) >> 1;

		part_2 = s2[row * 16 + col];

		uint_fast32_t part_3;
		part_3 = (r & 0x000000FC0000000) >> 30;

		row = ((part_3 & 0b100000) >> 4) | (part_3 & 1);
		col = (part_3 & 0b011110) >> 1;

		part_3 = s3[row * 16 + col];
		
		uint_fast32_t part_4;
		part_4 = (r & 0x00000003F000000) >> 24;

		row = ((part_4 & 0b100000) >> 4) | (part_4 & 1);
		col = (part_4 & 0b011110) >> 1;

		part_4 = s4[row * 16 + col];

		uint_fast32_t part_5;
		part_5 = (r & 0x000000000FC0000) >> 18;

		row = ((part_5 & 0b100000) >> 4) | (part_5 & 1);
		col = (part_5 & 0b011110) >> 1;

		part_5 = s5[row * 16 + col];
		
		uint_fast32_t part_6;
		part_6 = (r & 0x00000000003F000) >> 12;

		row = ((part_6 & 0b100000) >> 4) | (part_6 & 1);
		col = (part_6 & 0b011110) >> 1;

		part_6 = s6[row * 16 + col];

		uint_fast32_t part_7;
		part_7 = (r & 0x000000000000FC0) >> 6;

		row = ((part_7 & 0b100000) >> 4) | (part_7 & 1);
		col = (part_7 & 0b011110) >> 1;

		part_7 = s7[row * 16 + col];

		uint_fast32_t part_8;
		part_8 = (r & 0x00000000000003F) >> 0;

		row = ((part_8 & 0b100000) >> 4) | (part_8 & 1);
		col = (part_8 & 0b011110) >> 1;

		part_8 = s8[row * 16 + col];

		r = (part_1 << 4 * 7) |
			(part_2 << 4 * 6) |
			(part_3 << 4 * 5) |
			(part_4 << 4 * 4) |
			(part_5 << 4 * 3) |
			(part_6 << 4 * 2) |
			(part_7 << 4 * 1) |
			(part_8 << 4 * 0);

		uint_fast32_t r_f;
		r_f = 0;
		for (j = 0; j < 32; ++j) {
			if (r & (1UL << (32 - p[j])))
				r_f |= 1UL << (31 - j);
		}
		r = l0 ^ r_f;

		l0 = l;
		r0 = r;
	}

	uint64_t r_res;
	r_res = ((uint64_t)r0 << 32) | l0;
	
	uint64_t pi_res;
	pi_res = 0;
	for (i = 0; i < 64; ++i) {
		if (r_res & (1UL << (64 - pi[i])))
			pi_res |= 1UL << (63 - i);
	}

	return pi_res;
}

uint64_t
z0_des_dec(uint64_t block, uint64_t key)
{
	uint64_t keys[16];
	z0_des_schedule_keys(key, keys);

	uint64_t ip_res;
	ip_res = 0;

	uint_fast8_t i;
	for (i = 0; i < 64; ++i) {
		if (block & (1UL << (64 - ip[i])))
			ip_res |= 1UL << (63 - i);
	}

	uint_fast32_t l0;
	l0 = ip_res & 0xFFFFFFFF00000000;
	l0 = l0 >> 32;
	uint_fast32_t r0;
	r0 = ip_res & 0x00000000FFFFFFFF;	

	uint_fast32_t l;
	uint_fast32_t r;
	for (i = 0; i < 16; ++i) {
		l = r0;
		r = 0;

		uint_fast8_t j;
		for (j = 0; j < 48; ++j) {
			if (r0 & (1UL << (32 - e[j])))
				r |= 1UL << (47 - j);
		}
		r = r ^ keys[15 - i];
		
		uint_fast8_t row;
		uint_fast8_t col;

		uint_fast32_t part_1;
		part_1 = (r & 0x0000FC0000000000) >> 42;

		row = ((part_1 & 0b100000) >> 4) | (part_1 & 1);
		col = (part_1 & 0b011110) >> 1;

		part_1 = s1[row * 16 + col];

		uint_fast32_t part_2;
		part_2 = (r & 0x000003F000000000) >> 36;

		row = ((part_2 & 0b100000) >> 4) | (part_2 & 1);
		col = (part_2 & 0b011110) >> 1;

		part_2 = s2[row * 16 + col];

		uint_fast32_t part_3;
		part_3 = (r & 0x000000FC0000000) >> 30;

		row = ((part_3 & 0b100000) >> 4) | (part_3 & 1);
		col = (part_3 & 0b011110) >> 1;

		part_3 = s3[row * 16 + col];
		
		uint_fast32_t part_4;
		part_4 = (r & 0x00000003F000000) >> 24;

		row = ((part_4 & 0b100000) >> 4) | (part_4 & 1);
		col = (part_4 & 0b011110) >> 1;

		part_4 = s4[row * 16 + col];

		uint_fast32_t part_5;
		part_5 = (r & 0x000000000FC0000) >> 18;

		row = ((part_5 & 0b100000) >> 4) | (part_5 & 1);
		col = (part_5 & 0b011110) >> 1;

		part_5 = s5[row * 16 + col];
		
		uint_fast32_t part_6;
		part_6 = (r & 0x00000000003F000) >> 12;

		row = ((part_6 & 0b100000) >> 4) | (part_6 & 1);
		col = (part_6 & 0b011110) >> 1;

		part_6 = s6[row * 16 + col];

		uint_fast32_t part_7;
		part_7 = (r & 0x000000000000FC0) >> 6;

		row = ((part_7 & 0b100000) >> 4) | (part_7 & 1);
		col = (part_7 & 0b011110) >> 1;

		part_7 = s7[row * 16 + col];

		uint_fast32_t part_8;
		part_8 = (r & 0x00000000000003F) >> 0;

		row = ((part_8 & 0b100000) >> 4) | (part_8 & 1);
		col = (part_8 & 0b011110) >> 1;

		part_8 = s8[row * 16 + col];

		r = (part_1 << 4 * 7) |
			(part_2 << 4 * 6) |
			(part_3 << 4 * 5) |
			(part_4 << 4 * 4) |
			(part_5 << 4 * 3) |
			(part_6 << 4 * 2) |
			(part_7 << 4 * 1) |
			(part_8 << 4 * 0);

		uint_fast32_t r_f;
		r_f = 0;
		for (j = 0; j < 32; ++j) {
			if (r & (1UL << (32 - p[j])))
				r_f |= 1UL << (31 - j);
		}
		r = l0 ^ r_f;

		l0 = l;
		r0 = r;
	}

	uint64_t r_res;
	r_res = ((uint64_t)r0 << 32) | l0;
	
	uint64_t pi_res;
	pi_res = 0;
	for (i = 0; i < 64; ++i) {
		if (r_res & (1UL << (64 - pi[i])))
			pi_res |= 1UL << (63 - i);
	}

	return pi_res;
}

uint64_t                                     
z0_3des_enc(uint64_t block, uint64_t key[3]) 
{                                            
        block = z0_des_enc(block, key[0]);   
        block = z0_des_dec(block, key[1]);   
        block = z0_des_enc(block, key[2]);   
                                             
        return block;                        
}                                            
                                             
uint64_t                                     
z0_3des_dec(uint64_t block, uint64_t key[3]) 
{                                            
        block = z0_des_dec(block, key[2]);   
        block = z0_des_enc(block, key[1]);   
        block = z0_des_dec(block, key[0]);   
                                             
        return block;                        
}                                            
