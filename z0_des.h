#ifndef Z0_DES_H
#define Z0_DES_H

#include <stdint.h>

uint64_t
z0_des_enc(uint64_t, uint64_t);

uint64_t
z0_des_dec(uint64_t, uint64_t);

uint64_t                           
z0_3des_enc(uint64_t, uint64_t[3]);
                                   
uint64_t                           
z0_3des_dec(uint64_t, uint64_t[3]);

#endif
